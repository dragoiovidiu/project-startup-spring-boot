CREATE TABLE IF NOT EXISTS `password_reset_token` (
  `id` bigint(20) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PASSWORD_RESET_TOKEN` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;