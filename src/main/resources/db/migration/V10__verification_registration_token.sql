CREATE TABLE IF NOT EXISTS `verification_registration_token` (
  `id` bigint(20) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_VERIFY_USER` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;