package org.project.startup.notification.service;

import org.project.startup.notification.event.UserEvent;
import org.project.startup.notification.model.Notification;

public interface NotificationService {
	public void sendNotification(UserEvent notification);
}
