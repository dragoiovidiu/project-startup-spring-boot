package org.project.startup.api.service;

import javax.servlet.http.HttpServletRequest;

import org.project.startup.api.dto.TokenDTO;
import org.project.startup.api.model.AuthTokenInfo;

public interface Oauth2RestClientService {
	public AuthTokenInfo sendTokenRequest(HttpServletRequest httpRequest, TokenDTO dto);
}
