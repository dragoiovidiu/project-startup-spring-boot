package org.project.startup.api.controller;

/************************************************************************************************
 Spring
 ************************************************************************************************/
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.project.startup.api.dto.DTOInterface;
import org.project.startup.api.dto.MessageDTO;
import org.project.startup.api.dto.UserDTO;
import org.project.startup.api.util.MessageType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * @author ovidiu.dragoi
 *
 */
@RestController
@RequestMapping("/api")
public abstract class ApiController {

	@Value("${registration.url}")
	private String registrationUrl;

	/**
	 * returns the url where confirm the registration based on token
	 * 
	 * @param request
	 * @return
	 */
	protected String getAppUrl(HttpServletRequest request) {
		return registrationUrl;
		// return "http://" + request.getServerName() + ":" + request.getServerPort() +
		// request.getContextPath();
	}
}
