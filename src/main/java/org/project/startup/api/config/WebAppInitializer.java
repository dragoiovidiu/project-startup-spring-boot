package org.project.startup.api.config;

import javax.servlet.Filter;
import org.project.startup.api.filter.CORSFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { WebConfig.class };
	}
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
        @Override
        protected Filter[] getServletFilters() {
    	        Filter [] filters = {new CORSFilter()};
    	        return filters;
        }
} 
