package org.project.startup.data;

import com.lambdaworks.redis.*;

public class ConnectToRedis {

  public static void main(String[] args) {
    RedisClient redisClient = new RedisClient(
      RedisURI.create("redis://localhost:6379"));
    RedisConnection<String, String> connection = redisClient.connect();

    System.out.println("Connected to Redis");

    connection.close();
    redisClient.shutdown();
  }
}